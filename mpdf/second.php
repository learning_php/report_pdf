<?php
require_once __DIR__ . '/vendor/autoload.php';

$mpdf = new \Mpdf\Mpdf(['defaultPageNumStyle' => '1', 'margin_top' => 31]);
				$mpdf->showImageErrors = true;

// Define a document with default left-margin=30 and right-margin=10
// $mpdf = new \Mpdf\Mpdf([
// 	'margin_top' => 0,
// 	'margin_left' => 30,
// 	'margin_right' => 10,
// 	'mirrorMargins' => true
// ]);


// Buffer the following html with PHP so we can store it to a variable later
ob_start();
?>

<html>
<head>
	<style>
		@page {
		    size: auto;
		    odd-header-name: html_myHeader1;
		    even-header-name: html_myHeader2;
		    odd-footer-name: html_myFooter1;
		    even-footer-name: html_myFooter2;
		}
		.img-logo {
			background-color: #095285;
			margin: 5px 5px 5px 5px;
			width: 150px;			
		}
		.float-left {
			float: left;
		}
		#pdfLogo {
		  width: 150px; 
		  background-color: green;
		  margin: 0px;
		  padding: 0px;
		}
	</style>
</head>
<body>


<htmlpageheader name="myHeader1" style="display:none">
  <div id="" class="single-row" style="padding: 0px; margin: 0px; border-bottom: 1px solid #000000; padding-bottom: 5px;">
    <div class="float-left" style="width: 530px; padding: 0px; margin: 0px;">
      <div style="height: 56px; padding-left: 200px;" class="text-left">
        <h3 style="line-height: 56px; margin: 0px;">PT Calculator | Datasheet</h3>
      </div>
    </div>
    <div id="pdfLogo" class="float-left" style="">
      <img class="img-logo" src="images/logo.png" />
    </div>
  </div>
</htmlpageheader>

<div class="container" style="margin: 0; padding: 0; width: 100%;">
	<div id="#" class="single-row" style="width: 100%;">
	  <!-- <div style="width: 100%; height: 15px; background-color: blue; margin: 0px; padding: 0px;"></div> -->
	  <h3 style="margin-top: 0px;">Struture Type:&nbsp;&nbsp;Two Way Parking Garage</h3>
	</div>
</div>


<h1>Hello World</h1>

<?php 
$html = ob_get_contents();
ob_end_clean();
// send the captured HTML from the output buffer to the mPDF class for processing
$mpdf->WriteHTML($html);
/*##########################################*/
/* (END) PÁGINA 1 */
/*##########################################*/

/*##########################################*/
/* (START) NUEVA PÁGINA: NRO. 2 */
/*##########################################*/
ob_start();
$mpdf->AddPage();
?>
<!-- // Now the right-margin (inner margin on page 2) = 30 -->
<h1>Hello World 2</h1>



</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();
// send the captured HTML from the output buffer to the mPDF class for processing
$mpdf->WriteHTML($html);
// $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);
$mpdf->Output('prueba.pdf', \Mpdf\Output\Destination::INLINE);
?>